package structure;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Vector;

public class Edge  implements Serializable{
int refNode1; 
int refNode2;
int partition1, partition2;
int type;
int edgeID;

public Edge(int a, int b)
{
	refNode1=a;
	refNode2=b;
}
public Edge(int id, int a, int b)
{
	edgeID = id;
	refNode1=a;
	refNode2=b;
}
public Edge(int a, int b, int p1, int p2)
{
	refNode1=a;
	refNode2=b;
	partition1=p1;
	partition2=p2;
}
public Edge(int a, int b, int p1, int p2, int type)
{
	refNode1=a;
	refNode2=b;
	partition1=p1;
	partition2=p2;
	this.type=type;
}

public int getNode1() {
	return refNode1;
}
public void setNode1(int node1) {
	this.refNode1 = node1;
}
public int getNode2() {
	return refNode2;
}
public void setNode2(int node2) {
	this.refNode2 = node2;
}
public int getEdgeID() {
	return edgeID;
}
public void setEdgeID(int id) {
	this.edgeID = id;
}
}
