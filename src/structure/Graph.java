package structure;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Graph {
	 HashMap< Integer, Node> listOfNodes= new HashMap<Integer, Node>();;
	 HashMap< Integer, Edge> listOfEdges= new HashMap<Integer, Edge>();;

	 public void parseGraph(String graphFile) throws NumberFormatException, IOException
	 {
		 int countEdge=1;
		 String line="";
			BufferedReader graphReader = new BufferedReader(new FileReader(graphFile));
			while((line=graphReader.readLine())!=null)
			{
				
				if(!line.equals(""))
				{
					String[] parts = line.split("\\s");
					int refNode1= Integer.parseInt(parts[0]);
					int refNode2= Integer.parseInt(parts[1]);
					Node node1 = new Node(refNode1); 
					Node node2 = new Node(refNode2); 
					listOfNodes.put(refNode1, node1);
					listOfNodes.put(refNode2, node2);
					Edge edge = new Edge (countEdge,refNode1,refNode1);
					listOfEdges.put(countEdge, edge);
					countEdge++;
					//System.out.println(node1);
				}
				
			}
			graphReader.close();
			
			for (Map.Entry<Integer, Edge> entry : listOfEdges.entrySet())
	        {
	        	
				System.out.println(entry.getValue().refNode1);    
	         
	        }
	 }
	 public static void main (String [] args) throws NumberFormatException, IOException 
	 {
		 Graph g= new Graph ();
		 //g.parseGraph("data/file");
	 }
}
