package BLADYG;

import graphTools.GraphTools;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import structure.Edge;
import messageBLADYG.ClusterIdentificationMsg;
import messageBLADYG.MasterToWorkerMsg;
import messageBLADYG.WorkerToMasterMsg;
import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.routing.FromConfig;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent.MemberEvent;
import akka.event.Logging;
import akka.event.LoggingAdapter;


public class Master extends UntypedActor {


private final int nrOfWorkers;
private final int nrOfPartitions;
private final String graphFile;
private final String partFile;
private final String partMethod;
private final int nbInsertions;
private final int nbDeletions;
private final String TypeOfEdges;

int MsgCount=0;
static int runningWorkers=0;
long NumberOfExchangedNodeswithCorenessForInsertions=0, NumberOfExchangedNodeswithCorenessForDeletions=0;
long NumberOfUpdatedNodesForInsertions=0,MaxNumberOfUpdatedNodesForInsertions=0,MinNumberOfUpdatedNodesForInsertions=0;
long NumberOfUpdatedNodesForDeletions=0,MaxNumberOfUpdatedNodesForDeletions=0,MinNumberOfUpdatedNodesForDeletions=0;
Vector NbExchangedNodesForInsertions=new Vector();
Vector NbExchangedNodesForDeletions=new Vector();
long NumberOfMsg=0;

Hashtable hashCurrentOfNeighborsOfReachableNodes,hashFusionNeighborsOfReachableNodes,hashCurrentCorenessNeighborsOfReachableNodes,hashFusionCorenessNeighborsOfReachableNodes;

static   Hashtable<Integer, ActorRef>  InfoWorkers;

static int indiceEdges=0;
int countWorkers=0;
static int nrOfResults=0;
private final long start = System.currentTimeMillis();
private ActorRef workerRouter;
long startTime ;
Vector frontierEdges,runtimesInsertions,runtimesDeletions, randomEdges;
Hashtable<Integer,Integer> InvertedHashPartitions;
public static Hashtable<Integer,Integer> HashPartitions;

ActorRef backend = getContext().actorOf(FromConfig.getInstance().props(),"workerRouter");
LoggingAdapter log = Logging.getLogger(getContext().system(), this);
Cluster cluster = Cluster.get(getContext().system());
		
		
//subscribe to cluster changes
@Override
public void preStart() throws IOException, InterruptedException {
//#subscribe
cluster.subscribe(getSelf(), MemberEvent.class);
log.info("Initializing  (Step1)...");
startBLADYGComputation();
}

//re-subscribe when restart
@Override
public void postStop() {
		   cluster.unsubscribe(getSelf());
}

//Constructor of the Master		  
public Master(final int nrOfWorkers, int nrOfPartitions, String graphFile, String partFile, String partMethod, int nbInsertions, int nbDeletions, String TypeOfEdges) throws IOException 
{
	this.nrOfPartitions = nrOfPartitions;
	this.nrOfWorkers = nrOfWorkers;
	this.graphFile = graphFile;
	this.partFile = partFile;
	this.partMethod = partMethod;
	this.TypeOfEdges = TypeOfEdges;
	
	this.nbInsertions=nbInsertions;
	this.nbDeletions=nbDeletions;
	InfoWorkers=new Hashtable<Integer, ActorRef>();
	
	if(partMethod.toUpperCase().equals("METIS"))
	{
		InvertedHashPartitions= GraphTools.constructPartitionsMetis(partFile, graphFile, "frontieredgesfile",nrOfPartitions, "Part_");
		log.info("Distribution of the nodes over partitions: "+InvertedHashPartitions);
	}
	else if (partMethod.toUpperCase().equals("RANDOM"))
	{
		InvertedHashPartitions= GraphTools.constructPartitionsRandom(partFile, graphFile, "frontieredgesfile",nrOfPartitions, "Part_");
		log.info("Distribution of the nodes over partitions: "+InvertedHashPartitions);
	}
	if(TypeOfEdges.toUpperCase().equals("INTERNAL"))
	{
		System.out.println("Generate a set of randomly selected internal edges");
		frontierEdges=generateRandomEdges(nbInsertions,nbDeletions);		
	}
	else if(TypeOfEdges.toUpperCase().equals("RANDOM"))
	{
		System.out.println("Generate a set of randomly selected frontier edges");
		frontierEdges=GraphTools.generateFrontierNodesFromFile("frontieredgesfile",nbInsertions,nbDeletions);	
	}
	
	FileWriter f = new FileWriter(graphFile+ "-insertions-"+TypeOfEdges+"-"+nrOfWorkers);
	for(int i=0;i<nbInsertions;i++){
		Edge e=(Edge)frontierEdges.elementAt(i);
		f.write(e.getNode1()+" "+e.getNode2()+"\n");
	}
	f.close();
	
	runtimesInsertions=new Vector();
	runtimesDeletions=new Vector();
	log.info("The Master constructor is executed ...");
}
public Vector generateRandomEdges(int nbInsertions, int nbDeletions)
{
	Vector vRandomEdges=new Vector(); 
	Vector vRandomEdgesDeletions=new Vector(); 

	for (int i=0;i<nbInsertions;i++) 
	{
		int trouve =0, node1 =0, node2 =0, part1 =0, part2 =0; 
		do
		{
			node1=GraphTools.randInt(1,HashPartitions.size()-1);
			node2=GraphTools.randInt(1,HashPartitions.size()-1);
			
			if(HashPartitions.containsKey(node1))
				part1=(Integer)HashPartitions.get(node1);
			
			if(HashPartitions.containsKey(node2))
				part2=(Integer)HashPartitions.get(node2);
			
			
		}
		while((part1==0)||(part2==0)||(part1!=part2)||(node1==node2));
		
		vRandomEdges.addElement(new Edge(node1,node2,part1,part2,1));
		vRandomEdgesDeletions.addElement(new Edge(node1,node2,part1,part2,2));
		
		//System.out.println("Nodes "+node1+" et "+node2+" are chosed");
	}
	vRandomEdges.addAll(vRandomEdgesDeletions);
	return vRandomEdges;
}
public void startBLADYGComputation() throws InterruptedException
{
		log.info("Number of partitions: "+	nrOfPartitions);
		//Thread.sleep(5000);
		backend.tell(new String(""), getSelf());

	for (int start = 0; start < nrOfPartitions; start++) 
	{	
		//log.info("First BLADYG computation on worker "+(start+1));
		
		Master.runningWorkers++;
		backend.tell(new ClusterIdentificationMsg(), getSelf());
		Thread.sleep(1000);
	}
	InfoWorkers.put(0,getSelf());
}

public void onReceive(Object message) throws Exception {
	
	 if (message instanceof ClusterIdentificationMsg) 
	 {
	 	countWorkers++;
	 	InfoWorkers.put(countWorkers,getSender());
	 	if(countWorkers==nrOfWorkers)
	 	{
	 		for(int i=0;i<=nrOfWorkers;i++)
	 		{
	 			System.out.println("Worker "+i+" = "+InfoWorkers.get(i));
	 			InfoWorkers.get(i).tell(new MasterToWorkerMsg(i,"WORKERSINFO",InfoWorkers), getSelf());
	 		}
	 	}
	 }
	 else if(message instanceof WorkerToMasterMsg)
	 {
		 WorkerToMasterMsg obj = (WorkerToMasterMsg) message;
		 
		 if(obj.getOperationInfo().toUpperCase().equals("PREPARESTEP1"))
		 {
			 MsgCount++;
			 
			 if(MsgCount==nrOfWorkers)
			 {
				 System.out.println("Preparation for step 1: Done!");
				 
				 //Step 1
				 for(int i=0;i<=nrOfWorkers;i++)
			 		{
			 			//System.out.println("Worker "+i+" = "+InfoWorkers.get(i));
			 			InfoWorkers.get(i).tell(new MasterToWorkerMsg(i,"STEP1",null), getSelf());
			 		}
				 MsgCount=0;
			 }
			 
		 }
		 else if(obj.getOperationInfo().toUpperCase().equals("FINISHSTEP1"))
		 {
			 MsgCount++;
			 if(MsgCount==nrOfWorkers)
			 {
				 System.out.println("Step 1: Done!");
				 
				 
				 //Step 2
				 for(int i=0;i<=nrOfWorkers;i++)
			 		{
			 			//System.out.println("Worker "+i+" = "+InfoWorkers.get(i));
			 			InfoWorkers.get(i).tell(new MasterToWorkerMsg(i,"STEP2",null), getSelf());
			 		}
				 MsgCount=0;
				 
			 }
				 
		 }
		 else if(obj.getOperationInfo().toUpperCase().equals("FINISHSTEP2"))
		 {
			 MsgCount++;
			 if(MsgCount==nrOfWorkers)
			 {
				 System.out.println("Step 2: Done!");
			 }
		 }
		 else
		 {
			 
		 }
	 }
	 else 
	 {
		unhandled(message);
	 }
}
}
 
