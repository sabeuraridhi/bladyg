package BLADYG;

import java.util.Hashtable;

import messageBLADYG.*;
import scala.concurrent.duration.Duration;
import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.util.Timeout;
import akka.cluster.Cluster;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class Worker extends UntypedActor {
	 LoggingAdapter log = Logging.getLogger(getContext().system(), this);
	 Timeout timeout = new Timeout(Duration.create(20, "seconds"));
	 Cluster cluster = Cluster.get(getContext().system());
	 int workerID;
	 static   Hashtable<Integer, ActorRef>  InfoWorkersInWorker;
	

	
	@Override
	public void onReceive(Object message) throws Exception {	

		if (message instanceof ClusterIdentificationMsg) 
		{
			getSender().tell(new ClusterIdentificationMsg(), getSelf());
		}
		else if(message instanceof WorkerToWorkerMsg)
		{
			WorkerToWorkerMsg obj = (WorkerToWorkerMsg) message;
			 
			 if(obj.getOperationInfo().toUpperCase().equals(""))
			 {
				 
			 }
			 else if(obj.getOperationInfo().toUpperCase().equals(""))
			 {
				 
			 }
			 else
			 {
				 
			 }
		}
		else if(message instanceof MasterToWorkerMsg)
		{
			MasterToWorkerMsg obj = (MasterToWorkerMsg) message;
			if(obj.getOperationInfo().toUpperCase().equals("WORKERSINFO"))
			{
				InfoWorkersInWorker=(Hashtable<Integer, ActorRef>) obj.getObject();
				workerID=obj.getReceiverWorkerID();
				System.out.println("[Worker "+workerID+"]: Initialization - I got the workers Info HashMap");
				getSender().tell(new WorkerToMasterMsg(workerID,"PREPARESTEP1",null), getSelf());
			}
			else if(obj.getOperationInfo().toUpperCase().equals("STEP1"))
			{
				System.out.println("[Worker "+workerID+"]: Starting step 1");
				
				//Step 1
				
				
				
				getSender().tell(new WorkerToMasterMsg(workerID,"FINISHSTEP1",null), getSelf());
				
			}
			else if(obj.getOperationInfo().toUpperCase().equals("STEP2"))
			{
				System.out.println("[Worker "+workerID+"]: Starting step 2");
				
				
				
				getSender().tell(new WorkerToMasterMsg(workerID,"FINISHSTEP2",null), getSelf());

				
			}
		}
		else 
		{
			unhandled(message);
		}
		
		
	}
}
