package BLADYG;

import java.io.IOException;

public class Main {
	
	 public static void main(String[] args) throws InterruptedException, IOException {
			
		 	//Starting the workers
		 	WorkerMain.main(new String []{"1", "ec2machines","4"});
			WorkerMain.main(new String []{"2", "ec2machines","4"});
			WorkerMain.main(new String []{"3", "ec2machines","4"});
			WorkerMain.main(new String []{"4", "ec2machines","4"});
			/*
			WorkerMain.main(new String []{"5", "ec2machines","4"});
			WorkerMain.main(new String []{"6", "ec2machines","4"});
			WorkerMain.main(new String []{"7", "ec2machines","4"});
			WorkerMain.main(new String []{"8", "ec2machines","4"});
			
			WorkerMain.main(new String []{"9", "ec2machines","4"});
			WorkerMain.main(new String []{"10", "ec2machines","4"});
			WorkerMain.main(new String []{"11", "ec2machines","4"});
			WorkerMain.main(new String []{"12", "ec2machines","4"});
			WorkerMain.main(new String []{"13", "ec2machines","4"});
			WorkerMain.main(new String []{"14", "ec2machines","4"});
			WorkerMain.main(new String []{"15", "ec2machines","4"});
			WorkerMain.main(new String []{"16", "ec2machines","4"});
			*/
			//Starting the master
			MasterMain.main(new String []{"data/ds100_10000","data/randomPartitioning_10000_4","RANDOM","4","4","10", "RANDOM", "ec2machines"});

	 }
}
