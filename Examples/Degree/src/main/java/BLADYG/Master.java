package BLADYG;

import graphTools.GraphTools;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import structure.Edge;
import messageBLADYG.ClusterIdentificationMsg;
import messageBLADYG.MasterToWorkerMsg;
import messageBLADYG.WorkerToMasterMsg;
import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.routing.FromConfig;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent.MemberEvent;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import messageBLADYG.UserToMaster;
import messageBLADYG.WorkerToWorkerMsg;
import structure.Node;

public class Master extends UntypedActor {

    private final int nrOfWorkers;
    private final int nrOfPartitions;
    private final String graphFile;
    private final String partFile;
    private final String partMethod;
    private final int nbInsertions;
    private final int nbDeletions;
    private final String TypeOfEdges;

    int MsgCount = 0;
    static int runningWorkers = 0;
    long NumberOfExchangedNodeswithCorenessForInsertions = 0, NumberOfExchangedNodeswithCorenessForDeletions = 0;
    long NumberOfUpdatedNodesForInsertions = 0, MaxNumberOfUpdatedNodesForInsertions = 0, MinNumberOfUpdatedNodesForInsertions = 0;
    long NumberOfUpdatedNodesForDeletions = 0, MaxNumberOfUpdatedNodesForDeletions = 0, MinNumberOfUpdatedNodesForDeletions = 0;
    Vector NbExchangedNodesForInsertions = new Vector();
    Vector NbExchangedNodesForDeletions = new Vector();
    long NumberOfMsg = 0;

    Hashtable hashCurrentOfNeighborsOfReachableNodes, hashFusionNeighborsOfReachableNodes, hashCurrentCorenessNeighborsOfReachableNodes, hashFusionCorenessNeighborsOfReachableNodes;

    static Hashtable<Integer, ActorRef> InfoWorkers;

    static int indiceEdges = 0;
    int countWorkers = 0;
    static int nrOfResults = 0;
    private final long start = System.currentTimeMillis();
    private ActorRef workerRouter;
    long startTime;
    Vector frontierEdges, runtimesInsertions, runtimesDeletions, randomEdges;
    Hashtable<Integer, Integer> InvertedHashPartitions;
    public static Hashtable<Integer, Integer> HashPartitions;
    // to save all results from worker
    Vector results = new Vector();
    double degree;
    // router 
    ActorRef backend = getContext().actorOf(FromConfig.getInstance().props(), "workerRouter");
    LoggingAdapter log = Logging.getLogger(getContext().system(), this);
    //cluster
    Cluster cluster = Cluster.get(getContext().system());

//subscribe to cluster changes
    @Override
    public void preStart() throws IOException, InterruptedException {
//#subscribe
        cluster.subscribe(getSelf(), MemberEvent.class);
        log.info("Initializing");
        startBLADYGComputation();
    }

//re-subscribe when restart
    @Override
    public void postStop() {
        cluster.unsubscribe(getSelf());
        log.info("cluster to be stoped...");
    }

//Constructor of the Master		  
    public Master(final int nrOfWorkers, int nrOfPartitions, String graphFile, String partFile, String partMethod, int nbInsertions, int nbDeletions, String TypeOfEdges) throws IOException {
        this.nrOfPartitions = nrOfPartitions;
        this.nrOfWorkers = nrOfWorkers;
        this.graphFile = graphFile;
        this.partFile = partFile;
        this.partMethod = partMethod;
        this.TypeOfEdges = TypeOfEdges;

        this.nbInsertions = nbInsertions;
        this.nbDeletions = nbDeletions;
        InfoWorkers = new Hashtable<Integer, ActorRef>();

        if (partMethod.toUpperCase().equals("METIS")) {
            InvertedHashPartitions = GraphTools.constructPartitionsMetis(partFile, graphFile, "frontieredgesfile", nrOfPartitions, "Part_");
            log.info("Distribution of the nodes over partitions: " + InvertedHashPartitions);
        } else if (partMethod.toUpperCase().equals("RANDOM")) {
            InvertedHashPartitions = GraphTools.constructPartitionsRandom(partFile, graphFile, "frontieredgesfile", nrOfPartitions, "Part_");
            log.info("Distribution of the nodes over partitions: " + InvertedHashPartitions);
        }
        if (TypeOfEdges.toUpperCase().equals("INTERNAL")) {
            System.out.println("Generate a set of randomly selected internal edges");
            frontierEdges = generateRandomEdges(nbInsertions, nbDeletions);
        } else if (TypeOfEdges.toUpperCase().equals("RANDOM")) {
            System.out.println("Generate a set of randomly selected frontier edges");
            frontierEdges = GraphTools.generateFrontierNodesFromFile("frontieredgesfile", nbInsertions, nbDeletions);
        }

        FileWriter f = new FileWriter(graphFile + "-insertions-" + TypeOfEdges + "-" + nrOfWorkers);
        for (int i = 0; i < nbInsertions; i++) {
            Edge e = (Edge) frontierEdges.elementAt(i);
            f.write(e.getNode1() + " " + e.getNode2() + "\n");
        }
        f.close();

        runtimesInsertions = new Vector();
        runtimesDeletions = new Vector();
        log.info("The Master constructor is executed ...");
    }

    public Vector generateRandomEdges(int nbInsertions, int nbDeletions) {
        Vector vRandomEdges = new Vector();
        Vector vRandomEdgesDeletions = new Vector();
        for (int i = 0; i < nbInsertions; i++) {
            int trouve = 0, node1 = 0, node2 = 0, part1 = 0, part2 = 0;
            do {
                node1 = GraphTools.randInt(1, HashPartitions.size() - 1);
                node2 = GraphTools.randInt(1, HashPartitions.size() - 1);
                if (HashPartitions.containsKey(node1)) {
                    part1 = (Integer) HashPartitions.get(node1);
                }
                if (HashPartitions.containsKey(node2)) {
                    part2 = (Integer) HashPartitions.get(node2);
                }
            } while ((part1 == 0) || (part2 == 0) || (part1 != part2) || (node1 == node2));

            vRandomEdges.addElement(new Edge(node1, node2, part1, part2, 1));
            vRandomEdgesDeletions.addElement(new Edge(node1, node2, part1, part2, 2));
        }
        vRandomEdges.addAll(vRandomEdgesDeletions);
        return vRandomEdges;
    }
//methode to identify alla workers

    public void startBLADYGComputation() throws InterruptedException {
        log.info("Number of partitions: " + nrOfPartitions);
        for (int start = 0; start < nrOfPartitions; start++) {
            Master.runningWorkers++;
            backend.tell(new ClusterIdentificationMsg(), getSelf());
            Thread.sleep(1000);
        }
        InfoWorkers.put(0, getSelf());
    }
// methode to process all message recived from user or from workers

    public void onReceive(Object message) throws Exception {
        // message to identify if the workers is runing
        if (message instanceof ClusterIdentificationMsg) {
            countWorkers++;
            InfoWorkers.put(countWorkers, getSender());
            // display all worker in runing
            if (countWorkers == nrOfWorkers) {
                for (int i = 0; i <= nrOfWorkers; i++) {
                    System.out.println("Worker " + i + " = " + InfoWorkers.get(i));
                    InfoWorkers.get(i).tell(new MasterToWorkerMsg(i, "WORKERSINFO", InfoWorkers), getSelf());
                }
            }
        } //recive message from workers 
        else if (message instanceof WorkerToMasterMsg) {
            WorkerToMasterMsg obj = (WorkerToMasterMsg) message;
            // result of Graph degree
            // if all workers sends its respons to master that  write a message to informe
            // a user that the graph has been loaded
            if (obj.getOperationInfo().toUpperCase().equals("RSTDENSITE")) {
                MsgCount++;
                degree += Double.parseDouble(obj.getObject().toString());
                if (MsgCount == nrOfWorkers) {

                    log.info("Degre of the graph is   " + degree);
                    MsgCount = 0;
                    degree = 0;
                }

            } //result of Graph loading
            // if all workers sends its respons a master write a message to informe
            // a user that the graph has been loaded
            else if (obj.getOperationInfo().toUpperCase().equals("RSTLOADGRAPH")) {
                MsgCount++;
                if (MsgCount == nrOfWorkers) {
                    log.info("you graph has been loaded");
                    MsgCount = 0;
                }
            } /*
            in this methode we get where we can palce a node "n"
            after a user sent a node to master, the master get from 
            all workers the similarity between this node and all the partitions
            
            
             */ else if (obj.getOperationInfo().toUpperCase().equals("GETPARTION")) {
                MsgCount++;
                results.add(obj.getObject());
                if (MsgCount == nrOfWorkers) {
                    int nb_sim = 0;
                    // actorref that have max similarity between node n and all partion
                    ActorRef pos = null;
                    Node n = null;
                    for (int i = 0; i < results.size(); i++) {
                        Vector v = (Vector) results.get(i);
                        if (nb_sim < Integer.parseInt(v.get(1).toString())) {
                            nb_sim = Integer.parseInt(v.get(1).toString());
                            pos = (ActorRef) v.get(0);
                            n = (Node) v.get(2);
                        }

                    }

                    // sent a node to worker which have max similarity
                    pos.tell(new MasterToWorkerMsg(1, "ADDNODE", n), self());
                    results.clear();
                    MsgCount = 0;
                }
            } else {
                log.error("we d'ont have a methode for this message");
            }
        } // if the master recive a message from user
        else if (message instanceof UserToMaster) {
            UserToMaster obj = (UserToMaster) message;
            // if a user needs to load graph in cluster
            if (obj.getAction().equals("LOADGRAPH")) {
                // start all workers to load graph
                for (int i = 0; i < nrOfPartitions; i++) {
                    // log.info("load the partion num " + (i + 1));
                    backend.tell(new MasterToWorkerMsg(i, "LOADGRAPH", new String("Part_" + (i + 1))), getSelf());
                    Thread.sleep(1000);
                }
            } //if user needs to get degree of graph 
            else if (obj.getAction().equals("DEGREE")) {
                for (int i = 0; i < nrOfPartitions; i++) {

                    backend.tell(new MasterToWorkerMsg(i, "DEGREE", new String("Part_" + (i + 1))), getSelf());
                    Thread.sleep(1000);
                }
            } else if (obj.getAction().equals("get_partion_to_edge")) {
                Edge e = (Edge) obj.getParapetres();
                //send to all workers the node 
                for (int i = 0; i < nrOfPartitions; i++) {
                    backend.tell(new MasterToWorkerMsg(i, "get_partion_to_edge", e), getSelf());
                    Thread.sleep(1000);
                }

            } else if (obj.getAction().equals("UPDATE")) {
                Node n = (Node) obj.getParapetres();
                //send to all workers the node to get similarty with thier partion
                for (int i = 0; i < nrOfPartitions; i++) {
                    backend.tell(new MasterToWorkerMsg(i, "ADD_NODE_1", n), getSelf());
                    Thread.sleep(1000);
                }
            }
        } // dispatch  message from worker to others worker
        else if (message instanceof WorkerToWorkerMsg) {
            WorkerToWorkerMsg obj = (WorkerToWorkerMsg) message;
            for (int i = 0; i < nrOfPartitions; i++) {
                backend.tell(obj, getSelf());
                Thread.sleep(1000);
            }
        } else {
            unhandled(message);
        }
    }
}
