package BLADYG;

import messageBLADYG.*;
import scala.concurrent.duration.Duration;
import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.util.Timeout;
import akka.cluster.Cluster;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import structure.Edge;
import structure.Graph;
import structure.Node;

public class Worker extends UntypedActor {

    LoggingAdapter log = Logging.getLogger(getContext().system(), this);
    Timeout timeout = new Timeout(Duration.create(20, "seconds"));
    Cluster cluster = Cluster.get(getContext().system());
    int workerID;
    static Hashtable<Integer, ActorRef> InfoWorkersInWorker;
    Graph g = new Graph();

    @Override
    public void onReceive(Object message) throws Exception {
        // step to Identification  
        if (message instanceof ClusterIdentificationMsg) {
            getSender().tell(new ClusterIdentificationMsg(), getSelf());
        } // parse all action between master to workers 
        else if (message instanceof MasterToWorkerMsg) {
            MasterToWorkerMsg obj = (MasterToWorkerMsg) message;
            // if master sent a message to load a graph or partion of graph
            if (obj.getOperationInfo().toUpperCase().equals("LOADGRAPH")) {
                //get path of pation from message
                String path_file = obj.getObject().toString();
                workerID = obj.getReceiverWorkerID() + 1;
                //parse the file to get all vertices and edges
                Set<String> vertices = new HashSet<String>();
                g.parseGraph(path_file);
                HashMap< Integer, Node> listOfNodes = g.getListOfNodes();
                HashMap< Integer, Edge> listOfEdges = g.getListOfEdges();
                g.setPartion(workerID);

                double localdensite = listOfEdges.size() / listOfNodes.size();
                getSender().tell(new WorkerToMasterMsg(obj.getReceiverWorkerID(), "RSTLOADGRAPH", null), getSelf());

            } // if master sent a message to get degree
            else if (obj.getOperationInfo().toUpperCase().equals("DEGREE")) {

                if (g.getListOfEdges().size() > 0 && g.getListOfNodes().size() > 0) {
                    HashMap< Integer, Node> listOfNodes = g.getListOfNodes();
                    HashMap< Integer, Edge> listOfEdges = g.getListOfEdges();
                    //browse all node in local graph
                    double local_degre = 0;
                    Node n;
                    for (Map.Entry<Integer, Node> nodes : listOfNodes.entrySet()) {
                        n = nodes.getValue(); // get curent node from list
                        double node_degree = 0;// node_degree foreach node

                        //browse all edge to get node_degree foreach node
                        Edge e;
                        for (Map.Entry<Integer, Edge> edges : listOfEdges.entrySet()) {
                            e = edges.getValue();
                            // test if the curent node represent node1 or node2 in the curent edge
                            if (e.getNode1() == n.getNode() || e.getNode2() == n.getNode()) {
                                node_degree += 1;
                            }

                        }

                        local_degre += node_degree;
                    }

                    getSender().tell(new WorkerToMasterMsg(obj.getReceiverWorkerID(), "RSTDENSITE", local_degre), getSelf());
                } else {
                    log.error("No graph found" + workerID);
                }
            } // the message to identify similarity between node and all others partions  
            else if (obj.getOperationInfo().toUpperCase().equals("ADD_NODE_1")) {
                //get node from message
                Node n = (Node) obj.getObject();
                //get list of neighbors to the node
                HashMap<Integer, Node> listOfNeighbors = n.getListOfNeighbors();
                //get list of node in curent partion
                HashMap<Integer, Node> listeofnodes = g.getListOfNodes();
                int nbofconnections = 0;
                for (Map.Entry<Integer, Node> nb : listOfNeighbors.entrySet()) {
                    for (Map.Entry<Integer, Node> ng : listeofnodes.entrySet()) {
                        if (nb.getValue().getNode() == ng.getValue().getNode()) {
                            nbofconnections++;
                        }
                    }
                }
                // store in the vector a work which have a max similarity and number of similarity
                Vector rst = new Vector();
                rst.add(getSelf());
                rst.add(nbofconnections);
                rst.add(n);
                getSender().tell(new WorkerToMasterMsg(workerID, "GETPARTION", rst), getSelf());

            } // if worker get addnode message from master he saves this node in local graph
            else if (obj.getOperationInfo().toUpperCase().equals("ADDNODE")) {
                //get node from message
                Node n = (Node) obj.getObject();
                //get list of neighbors to the node
                HashMap<Integer, Node> listOfNeighbors = n.getListOfNeighbors();
                // add the  node to graph "G"
                g.addNode(n);
                for (Map.Entry<Integer, Node> nb : listOfNeighbors.entrySet()) {
                    // if the local partion contains curent Neighbor
                    if (g.getListOfNodes().containsKey(nb.getKey())) {
                        Node nc = g.getNode(nb.getKey());
                        // add the node "n" to list of neighbor of current node
                        nc.addNeighbor(n.getNode(), n);
                        g.setNode(nc);
                        g.addedge(n, nc);

                    } else //curent Neighbor in anthors partion
                    {
                        // in this variable we preparte node to added to graph and its partion
                        n.setNode(workerID);
                        getSender().tell(new WorkerToWorkerMsg(workerID, workerID, "addnode", n), getSelf());
                    }
                }

            } else {
                //  log.error("we d'ont have a methode for this message from ");
            }

        } else if (message instanceof String) {
            log.error((String) message);
        } // if a worker recive messages from its neighbors to store  some links
        //
        else if (message instanceof WorkerToWorkerMsg) {
            WorkerToWorkerMsg obj = (WorkerToWorkerMsg) message;

            Node n = (Node) obj.getObject();

            //get list of neighbors to the node
            HashMap<Integer, Node> listOfNeighbors = n.getListOfNeighbors();
            // add the  node to graph "G"
            g.addNode(n);
            for (Map.Entry<Integer, Node> nb : listOfNeighbors.entrySet()) {
                // if the local partion contains curent Neighbor
                if (g.getListOfNodes().containsKey(nb.getKey())) {
                    Node nc = g.getNode(nb.getKey());
                    //add the node "n" to list of neighbor of current node
                    nc.addNeighbor(n.getNode(), n);
                    g.setNode(nc);
                    //add the link with tow partion one for curent partion and other to node "n"
                    g.addedge(n, nc, n.getPartition(), workerID);

                }
            }

        } else {
            unhandled(message);
        }

    }
}
